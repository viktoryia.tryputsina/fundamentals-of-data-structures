package main.java.com.epam.data_structures;

import java.util.Arrays;

public class CustomArrayList {

    private static final int DEFAULT_SIZE = 10;
    private int[] elements;
    private int currentIndex;

    public CustomArrayList() {
        elements = new int[DEFAULT_SIZE];
    }

    public void add(int element) {
        if (currentIndex >= elements.length) {
            doubleArraySize();
        }
        elements[currentIndex++] = element;
    }

    public void add(int element, int index) {
        validateIndex(index);

        if (index >= elements.length) {
            doubleArraySize();
        }
        System.arraycopy(elements, index, elements, index + 1, elements.length - currentIndex);

        elements[index] = element;
        currentIndex++;
    }

    private void validateIndex(int index) {
        if (index >= elements.length) {
            throw new ArrayIndexOutOfBoundsException("Size = " + elements.length + ", Index = " + index);
        }
    }

    private void doubleArraySize() {
        int newSize = elements.length * 2;
        int[] newElements = new int[newSize];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;
    }

    public void removeByIndex(int index) {
        validateIndex(index);

        System.arraycopy(elements, index + 1, elements, index, elements.length - index - 1);
        currentIndex--;
    }

    public void removeByValue(int value) {
        int index = 0;
        boolean isValueExists = false;

        while (!isValueExists && index < elements.length) {
            if (elements[index] == value) {
                isValueExists = true;
            } else {
                index++;
            }
        }

        if (isValueExists) {
            removeByIndex(index);
        }
    }

    public void replace(int element, int index) {
        validateIndex(index);
        elements[index] = element;
    }

    @Override
    public String toString() {
        return Arrays.toString(elements);
    }
}
