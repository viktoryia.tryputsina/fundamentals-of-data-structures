package main.java.com.epam.data_structures;

public class Main {
    public static void main(String[] args) {
        testLinkedList();
        testArrayList();
    }

    public static void testArrayList() {
        CustomArrayList list = new CustomArrayList();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        System.out.println(list.toString());

        list.add(10);
        System.out.println(list.toString());

        list.add(42, 2);
        System.out.println(list.toString());

        list.removeByIndex(2);
        System.out.println(list.toString());

        list.removeByValue(3);
        System.out.println(list.toString());

        list.removeByValue(22);
        System.out.println(list.toString());

        list.add(33);
        System.out.println(list.toString());

        list.replace(11, 10);
        System.out.println(list.toString());
    }

    private static void testLinkedList() {
        CustomLinkedList list = new CustomLinkedList();
        list.add(3);
        list.add(2);
        list.add(6);

        print(list);

        list.add(5, 1);
        print(list);

        list.removeByIndex(0);
        print(list);

        list.removeByValue(6);
        print(list);
    }

    private static void print(CustomLinkedList list) {
        for (int i = 0; i < list.getSize(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();
    }
}
